import
  tables, macros, strutils, algorithm, typetraits, sequtils,
  fowltek/maybe_t
export typetraits.name, tables

when not defined(semistatic):
  type semistatic[T] = static[T] or T
  template isStatic* (xpr): bool = compiles(static(xpr))

type
  TEntDestructor* = proc(entity:PEntity){.nimcall.}
  PTypeinfo* = ref object
    ## an aggregate of components
    size: int
    messages: seq[RT_Message]
    initializers*, destructors*: seq[TEntDestructor]
    errors*: TMaybe[seq[string]]
    when defined(TypeinfosHaveComponentSet):
      componentSet*: seq[int]
    when false:
      components: seq[TMaybe[tuple[offset: int]]]
    else:
      components: seq[bool]
      offsets: seq[int]

  TMessageKind* = enum mNope = 0, mUnicast, mMulticast
  RT_Message = object
    case kind*: TMessageKind
    of mNope:
      nil
    of mUnicast:
      f*: pointer
    of mMulticast:
      fs*: seq[pointer]

  PEntity* = ref TEntity
  TEntity* = object of TObject
    data*: cstring
    ty*: PTypeinfo
    id*: int

proc `$`* (m:RTMessage):string =
  result = "("
  result.add($ m.kind)
  case m.kind
  of mUnicast:
    result.add " set="
    result.add($ not m.f.isNil)
  of mMulticast:
    result.add " len="
    result.add($ m.fs.len)
  else:
    discard
  result.add")"


type
  # compile time message information
  TMessage = object
    id: int
    multicast: bool
    params: PNimrodNode

var
  ## compiletime message info
  messageIDs {.compileTime,global.} = initTable[string,TMessage]()
  m_id_counter {.compileTime.} = 0
  
var
  ## runtime message info
  rt_msgs_names* = initTable[string,int]()
  rt_msgs_multicast* : seq[bool] = newSeq[bool](512)
rt_msgs_multicast.setLen 0

template dump_messages* : stmt {.immediate.} =
  # dump message info at compile time
  static:
    echo m_id_counter, " messages:"
    for name,m in pairs(messageIDs):
      echo "  #",m.id, " ",name, " ", (if m.multicast: "multicast" else: "unicast"), " ", repr(m.params)
template dump_messages_rt* (id,name,isMulticast; body:stmt): stmt{.immediate.}=
  # dump message info at run-time, results will be in any order
  for name,id in pairs(rt_msgs_names):
    let isMulticast = rt_msgs_multicast[id]
    body

proc ensureLen* [T] (S:var seq[T]; L:int) =
  if S.len < L:
    S.setLen L

proc collect_argnames (formal_params: PNimrodNode): seq[string] {.compiletime.} =
  assert formal_params.kind == nnkFormalParams
  result = @[]
  for i in 1 .. <len(formal_params):
    for ii in 0 .. len(formal_params[i])-3:
      result.add($ formal_params[i][ii].ident)


template voidExpr* (x): expr = not compiles(type(x))
  # hack to determine if an expression is void


macro multicast* (fun): stmt =
  when defined(Debug): echo treerepr fun
  let f = fun.copyNimTree
  f.params.insert(1, newIdentDefs("entity".ident, "PEntity".ident))

  var default_case = 
    if f.body.kind != nnkEmpty: Just(f.body.copyNimTree) else: Nothing[PNimrodNode]()

  let name = ($basename(f.name).ident).normalize
  if messageIDs.hasKey(name):
    echo "entity failure ", name, " is already declared! new params: ($#) old params: ($#)".format(
      f.params.repr, messageIDs[name].params.repr)
    quit 1

  let m_ty = "MSG_"&name
  let m_id = m_id_counter
  m_id_counter += 1
  when defined(debug):
    echo "New multicast message #", $m_id, ": ", name, " ", repr(f.params)
  
  var msg: TMessage
  msg.id = m_id
  msg.multicast = true
  msg.params = f.params
  messageIDs[name] = msg
  
  let arg_names = collect_argnames(f.params)
  
  #result body should be something like
  #  if entity.ty.messages[m_id].kind == mMulticast:
  #    for f in entity.ty.messages[m_id].fs.items:
  #      cast[MSG_name]( f )( arguments... )
  let xp = parseStmt("""
if entity.ty.messages[$1].kind == mMulticast:
  for f in entity.ty.messages[$1].fs.items:
    cast[$2](f)($3)
""".format( m_id, m_ty, arg_names.join(",") ))[0]
  if default_case.has:
    xp.add newNimNode(nnkElse).add(default_case.val)
  f.body = newStmtList(xp)
  
  result = newStmtList()
  
  result.add newNimNode(nnkTypeSection).add(
    newNimNode(nnkTypeDef).add(
      ident(m_ty).postfix("*"), 
      newEmptyNode(),
      newNimNode(nnkProcTy).add(f.params, newNimNode(nnkPragma).add(ident"nimcall"))))
  result.add f
  result.add parseExpr("rt_msgs_multicast.ensureLen("& $(1+m_id) &")")
  result.add parseExpr("rt_msgs_multicast["& $m_id & "] = true")
  result.add parseExpr("rt_msgs_names[\"$#\"] = $#".format(name, m_id))
  
  when defined(Debug):
    result.repr.echo

proc compileTimeMessageExists* (messageName:string): bool {.compileTime.} =
  return messageIDs.hasKey(messageName)
proc compileTimeMessageInfo* (messageName: string): TMessage {.compileTime.} =
  return messageIDs[messageName]
proc acquireNextMessageID : int {.compileTime.} =
  result = m_id_counter
  m_id_counter += 1
proc declareMessage (name:string, isMulticast:bool, params:PNimrodNode):int {.compileTime.} =
  result = acquireNextMessageID() 
  messageIDs[name] = TMessage(id: result, multicast: isMulticast, params: params)


macro unicast* (fun): stmt =
  let f = fun.copyNimTree
  f.params.insert(1, newIdentDefs("entity".ident, "PEntity".ident))

  let default_case = 
    if f.body.kind == nnkEmpty: Nothing[PNimrodNode]() else: Just(f.body)

  let name = ($basename(f.name).ident).normalize
  if compileTimeMessageExists(name):
    quit "entoody failure: "& name& " is being redeclared! new params: ("& f.params.repr& ") old params: ("& compileTimeMessageInfo(name).params.repr& ")"
  
  let msg_type_name = "MSG_"&name

  var arg_names : seq[string] = @[]
  for idx in 1 .. <len(f.params):
    for ii in 0 .. len(f.params[idx])-3:
      arg_names.add($ f.params[idx][ii])
    
  #let is_void = f.params[0].kind == nnkEmpty or (f.params[0].kind == nnkIdent and ($f.params[0]).tolower == "void")
  
  #let m_id = m_id_counter
  #m_id_counter += 1
  let m_id = declareMessage(name, false, f.params)
  when defined(debug):
    echo name, ": id=", m_id, " (", repr(f.params),")"

  #if messageIDS.isnil:
  #  echo "ITS NIL"
  #  quit 1
  
  
  #the code i need here:
  #  if entity.ty.messages[m_id].kind == mUnicast:
  #    cast[MSG_ name](entity.ty.messages[m_id].f)(entity, args...)
  
  let message_body = """
if entity.ty.messages[$1].kind == mUnicast:
  when voidExpr(cast[$2](entity.ty.messages[$1].f)($3)):
    cast[$2](entity.ty.messages[$1].f)($3)
  else:
    return cast[$2](entity.ty.messages[$1].f)($3)
""".format(
    m_id, msg_type_name, arg_names.join(",")
  )
  let mb = parseStmt(message_body)[0]
  if default_case.has:
    mb.add newNimNode(nnkElse).add(default_case.val)
    echo treerepr(mb)

  f.body = newStmtList(mb)
  
  result = newStmtList()
  #type MSG_get_pos = proc(entity:PEntity): TPoint2d {.nimcall.}
  result.add newNimNode(nnkTypeSection).add(
    newNimNode(nnkTypeDef).add(
      ident(msg_type_name).postfix("*"), 
      newEmptyNode(),
      newNimNode(nnkProcTy).add(f.params, newNimNode(nnkPragma).add(ident"nimcall"))))
  result.add f
  #some message info is needed at runtime
  result.add parseExpr("rt_msgs_multicast.ensureLen("& $(1+m_id) &")")
  result.add parseExpr("rt_msgs_multicast["& $m_id & "] = false")
  result.add parseExpr("rt_msgs_names[\"$#\"] = $#".format(name, m_id))
  when defined(Debug):
    result.repr.echo

proc msg_is_multicast* (msg:string):bool{.compileTime.} =
  messageIDs[msg.normalize].multicast
proc msg_is_unicast* (msg:string): bool {.compileTime.} =
  not messageIDs[msg.normalize].multicast

discard """ proc get_pos : float {.unicast.}
#proc get_pos : string  {.unicast.} """

#echo messageID("FOO")

type
  TUnicastMsg = tuple[f:pointer, weight:int32]
  PComponentInfo* = ref object
    id*: int
    name*: string
    size*: int
    initializer*,destructor*: TEntDestructor
    unicast_messages: TTable[int, TUnicastMsg]
    multicast_messages: TTable[int, pointer]
    
    requiredComponents,conflictingComponents:seq[int]
    
  
var 
  component_id_counter = 0
  all_declared_components*{.global.}: seq[PComponentInfo] = @[]

proc numComponents* : int = all_declared_components.len
proc componentID* (T:typedesc): int 
proc componentInfo* (id: int): PComponentInfo = all_declared_components[id]
proc componentInfo* (T:typedesc): PComponentInfo = componentInfo(componentID(T))#all_declared_components[componentID(T)]

proc requiresComponent* (a,b: typedesc) =
  componentInfo(a).requiredComponents.add(componentID(b))
proc conflictsWithComponent* (a,b: typedesc) =
  componentInfo(a).conflictingComponents.add(componentID(b))

proc setInitializer* (comp:typedesc; f:TEntDestructor) =
  componentInfo(comp).initializer = f
proc setDestructor* (comp:typedesc; f:TEntDestructor) =
  componentInfo(comp).destructor = f

proc new_component_id (T:typedesc): int =
  result = component_id_counter
  inc component_id_counter
  
  let ci = PComponentInfo(
    id: result,
    name: name(T),
    size: sizeof(T),
    unicast_messages: initTable[int, TUnicastMsg](),
    multicast_messages: initTable[int,pointer](),
    required_components: @[],
    conflicting_components: @[]
  )
  
  if all_declared_components.len < result+1:
    all_declared_components.setLen result+1
  all_declared_components[result] = ci
  
  when defined(Debug):
    echo ci.name, " declared."
    
proc componentID* (T:typedesc): int =
  let ID {.global.} = new_component_id(T)
  return ID


proc findMessage* (ci:PComponentInfo; msg:semistatic[string]): TMaybe[pointer] =
  when isStatic(msg):
    when msg_is_multicast(msg):
      return ci.multicast_messages[messageID(msg)].maybe
    else:
      return ci.unicast_messages[messageID(msg)].f.maybe
  else:
    let m_id = rt_msgs_names[msg.normalize]
    if rt_msgs_multicast[m_id]:
      return ci.multicast_messages[m_id].maybe
    else:
      return ci.unicast_messages[m_id].f.maybe

proc messageID* (m:string): int {.compileTime.} = messageIDs[m.normalize].id

proc p* [t] (some:t):t {.inline.}=
  echo some
  return some

macro defMsg* : stmt {.immediate.} =
  # Implements a message for a component
  # 
  # Arguments:
  #   component: typedesc, 
  #   message: static[string],
  #   weight: int = 0,
  #   function: proc(arg1: TArg1, ..)
  #
  # Usage:
  #   def_msg(THealthComponent, "TakeDamage") do (damage: int):
  #     # `entity: PEntity` is injected in the params
  #     entity[THealthComponent].hp -= damage
  #     if entity[THealthComponent].hp < 0:
  #       entity.die
  #
  let cs = callsite()
  assert cs.kind == nnkCall 
  var
    component: PNimrodNode
    msg: int
    weight = 0
    function: PNimrodNode
    functionIDX = 3
  
  component = cs[1]
  let msg_name = ($cs[2]).normalize
  msg = messageID(msg_name)
  
  if cs.len > 4:
    weight = cs[3].intval.int
    functionIDX = 4
  if functionIDX != cs.len-1:
    echo treerepr(cs)
    quit "Malformed arguments for defMsg "& $functionidx & ", "& $cs.len
  if cs[functionIDX].kind == nnkStmtList:
    function = newProc(procType = nnkLambda, body = cs[functionIDX])
  else:
    let f = cs[functionIDX].copyNimTree
    function = newNimNode(nnkLambda)
    f.copyChildrenTo function
  
  # inject self:PEntity
  function.params.insert(1, newIdentDefs("entity".ident, "PEntity".ident))
  
  let blck_stmt = newStmtList()
  blck_stmt.add newNimNode(nnkLetSection).add(
    newIdentDefs(
      ident"cfunc",
      ident("MSG_"&msg_name),
      function
    )
  )
  if messageIDs[msg_name].multicast:
    blck_stmt.add parseStmt("componentInfo($1).multicast_messages[$2] = cast[pointer](cfunc)".format(
      component.repr, msg))
  else:
    blck_stmt.add parseStmt("componentInfo($1).unicast_messages[$2] = (cast[pointer](cfunc), $3'i32)".format(
      component.repr, msg, weight))
  result = newBlockStmt(blck_stmt)
  
  when defined(debug):
    echo "defMsg($1,$2) result:".format(component.repr, msg_name)  
    result.repr.echo

# template msgImpl*(component, msg_name, func): stmt {.immediate.} =
#   block:
#     let cfunc: `MSG msg_name` = func
#     const msg_id = messageID(astToStr(msg_name))
#     echo "Declaring (",msg_id," ",astToStr(msg_name), ") for ", componentInfo(component).name
#     when msg_is_unicast(astToStr(msg_name)):
#       componentInfo(component).unicast_messages[msg_id] = (cast[pointer](cfunc),0'i32)
#     else:
#       componentInfo(component).multicast_messages[msg_id] = cast[pointer](cfunc)


template dump_components* : stmt {.immediate.} =
  echo all_declared_components.len, " declared components:"
  for comp in all_declared_components:
    echo "  #", comp.id, ": ", comp.name


type
  TComponentSet* = seq[int]

proc componentIDs* (components: varargs[int,`componentID`]): TComponentSet = 
  result = @components
  result.sort(cmp[int])
proc isValid* (ty:PTypeinfo): bool =
  not ty.errors.has

proc newSeq* [T] (size: int; default: T): seq[T] =
  newSeq result, size
  for i in 0 .. < size:
    result[i] = default

proc newTypeinfo* (components: TComponentSet): PTypeinfo =
  # aggregates components so an entity can be instantiated.
  # each component has initializers, destructors and responds
  # to messages, PTypeinfo just collects them together.
  # resulting type may not be valid, check with isValid(ty)
  result = PTypeinfo()
  
  let n_my_comps = components.len
  if n_my_comps == 0:
    result.errors = Just(@["Entity has 0 components"])
    return
  
  result.initializers.newSeq 0
  result.destructors.newSeq 0
  result.errors.val.newSeq 0
  
  let n_comps = all_declared_components.len
  let n_msgs = rt_msgs_multicast.len #m_id_counter
  #echo "n_msgs: ", n_msgs
  result.messages.newSeq n_msgs
  var unicast_weights = newSeq[int](n_msgs, -1)
  result.offsets.newSeq n_comps
  result.components.newSeq n_comps
  
  # calculate offset for component data
  var offset = 0
  var requiredComps, conflictComps: seq[int]
  requiredComps.newseq 0
  conflictComps.newseq 0
  
  when defined(TypeinfosHaveComponentSet):
    result.componentSet = components
  
  for c_id in components:
    let c = componentInfo(c_id)
    result.offsets[c_id] = offset
    result.components[c_id] = true
    inc offset, c.size
    
    # collect initializers and destructors
    if not c.initializer.isNil: result.initializers.add c.initializer
    if not c.destructor.isNil: result.destructors.add c.destructor
    
    requiredComps.add c.requiredComponents
    conflictComps.add c.conflictingComponents
    
    for id, f in c.unicast_messages.pairs:
      template this_message: expr = result.messages[id]
      
      if this_message.kind == mNope or f.weight > unicast_weights[id]:
        this_message = RT_Message(kind: mUnicast, f: f.f)
        unicast_weights[id] = f.weight
   
    for id, f in c.multicast_messages.pairs:
      if result.messages[id].kind == mNope: 
        result.messages[id] = RT_Message(kind: mMulticast, fs: @[f])
      elif result.messages[id].kind == mMulticast:
        result.messages[id].fs.add f
      else: 
        echo "multicast id class ", id, " with ", result.messages[id]

  # offset ends up being the size we need to allocate for the datas
  result.size = offset
  
  requiredComps = deduplicate(requiredComps)
  requiredComps.sort cmp[int]
  for c_id in requiredComps:
    if not result.components[c_id]:
      result.errors.val.add "Component $# is required" % componentInfo(c_id).name
      
  conflictComps = deduplicate(conflictComps)
  conflictComps.sort cmp[int]
  for c_id in conflictComps:
    if result.components[c_id]:
      result.errors.val.add "Component $# is in conflict" % componentInfo(c_id).name
  
  result.errors.has = result.errors.val.len > 0


proc getModifiedComponentSet* (ty:PTypeinfo; add,remove:openarray[int]): TComponentSet =
  
  when defined(TypeinfosHaveComponentSet):
    result = ty.componentSet
  else:
    newseq result,0
    when false:
      for idx, c in ty.components.pairs:
        if c.has: result.add idx
    else:
      for idx, c in ty.components.pairs:
        if c: result.add idx

  result.add(add)
  result.sort(system.cmp[int])
  result = result.deduplicate
  for id in remove:
    let idx = result.find(id)
    if idx != -1:
      system.delete result,idx




proc getSize* (ty: PTypeinfo): int = ty.size

proc allocate* (ty:PTypeinfo): cstring = cast[cstring](alloc0(ty.size))
  
proc initialize* (entity:PEntity) =
  for f in entity.ty.initializers: f(entity)
proc destroy* (entity:PEntity) =
  # run destructors and dealloc entity data
  if entity.data.isNil: return
  for f in entity.ty.destructors: f(entity)
  dealloc(entity.data)
  entity.data = nil

type 
  RBadEntity* = ref EBadEntity
  EBadEntity* = object of EBase
    errors*: TMaybe[seq[string]]

proc instantiate* (ty:PTypeinfo, initialize = true): PEntity =
  # allocates and initializes a typeinfo
  if ty.errors.has: 
    var x = newException(EBadEntity, "typeinfo has errors").RBadEntity
    x.errors = Just(ty.errors.val)
    raise x

  let dat = ty.allocate
  if dat.isNil:
    raise newException(EBadEntity, "failed to allocate memory")
  
  new(result) do (ent:PEntity):
      ent.destroy
  result.ty = ty
  result.data = dat
  if initialize:
    result.initialize




type  
  PDomain* = ref object
    # Domain is a cache of entity types
    types*: TTable[TComponentSet, PTypeinfo] 

proc newDomain* : PDomain =
  PDomain(types: initTable[TComponentSet,PTypeinfo]())

proc getTypeinfo* (dom:PDomain; components:varargs[int, `componentID`]): PTypeinfo =
  var components = @components
  components.sort cmp[int]
  result = dom.types[components]
  if result.isNil:
    result = newTypeinfo(components)
    dom.types[components] = result


proc modifyComponents* (dom: PDomain; ty:PTypeinfo; 
      add, remove: openarray[int] = []): PTypeinfo =
  let cs = getModifiedComponentSet(ty, add, remove)
  result = dom.getTypeinfo(cs)


proc newEntity* (dom:PDomain; components:varargs[int,`componentID`]): PEntity =
  dom.getTypeinfo(components).instantiate(true)

proc get* (entity: PEntity; ty: typedesc): var ty {.inline.} =
  # access the data for a component
  when true:
    cast[var ty](entity.data[entity.ty.offsets[componentID(ty)]].addr)
  else:
    cast[var ty](entity.data[entity.ty.components[componentID(ty)].val.offset].addr)

proc `[]`* (ent:PEntity; ty:typedesc):var ty {.inline.}= ent.get(ty)
  # shortcut for `get(ent,ty)` 
proc `[]=`* (ent:PEntity; ty:typedesc; val:ty) {.inline.} = ent.get(ty) = val
  # shortcut for `get(ent,ty) = val`


proc hasComponent* (ty:PTypeinfo; comp:int): bool = 
  when true:
    return ty.components[comp]
  else:
    return ty.components[comp].has
proc hasComponent* (entity:PEntity; component:int):bool{.inline.}=
  entity.ty.hasComponent(component)
proc hasComponent* (entity:PEntity; component:typedesc):bool{.inline.}=
  entity.ty.hasComponent(componentID(component))


proc changeType* (entity:PEntity; new_type:PTypeinfo) =
  var new_ent = new_type.instantiate(false)
  let old_type = entity.ty 
  # for each component (x)
  #  if old_type.hasComponent(x):
  #    if new_type.hasComponent(x):
  #      copymem new_ent[X], entity[X], componentInfo(X).size
  #    else:
  #      run X's destructor on entity
  #  else:
  #    if new_type.hasComponent(x):
  #      run X's initializer on new_entity
  #    else:
  #      do nothing
  # then, swap new_ent's data with entity
  #  swap entity.data, new_ent.data
  #  swap entity.ty, new_ent.ty
  for comp in all_declared_components.items:
    let cid = comp.id
    let newty_has_it = new_type.hasComponent(cid)
    if old_type.hasComponent(cid):
      if newty_has_it:
        when true:
          copymem(
            new_ent.data[new_type.offsets[cid]].addr,
            entity.data[old_type.offsets[cid]].addr,
            comp.size
          )
        else:
          copymem(
            new_ent.data[new_type.components[cid].val.offset].addr,
            entity.data[old_type.components[cid].val.offset].addr,
            comp.size
          )
      else:
        if not comp.destructor.isNil: comp.destructor(entity)
    else:
      if newty_has_it:
        if not comp.initializer.isNil: comp.initializer(new_ent)
      else:
        discard

  swap entity.data, new_ent.data
  swap entity.ty, new_ent.ty

  # hard destroy new_ent (now old entity data)
  # do not use destroy() as it would run destructors that we've already ran
  # no, all we need a dealloc here
  dealloc new_ent.data
  new_ent.data = nil

proc changeComponents* (dom:PDomain; entity:PEntity; add,remove:openarray[int]) =
  let ty = dom.modifyComponents(entity.ty, add, remove)
  entity.changeType(ty)


## all the testing is done in another module
## this all has to work from outside this module


