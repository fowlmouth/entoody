## entitty tests
import entoody

## first define some basic components
import basic2d
type
  Position = object
    p: TPoint2d

proc test1 (components: var seq[int]) {.multicast.}
 # each component will add its id to components

defMsg(Position, test1) do (components: var seq[int]):
  components.add(componentID(Position))

proc test_unhandled (): int {.unicast.} =
  echo "test_unhandled was not handled"
  result = 42
proc test_unhandled_m {.multicast.} =
  echo "test_unhandled_m was not handled"

type
  Velocity = object
    v: Vector2d

proc get_vel* : Vector2d {.unicast.}
proc set_vel* (v:Vector2d){.unicast.}

defMsg(Velocity, get_vel) do -> Vector2d:
  entity[Velocity].v
defMsg(Velocity, set_vel) do (v:Vector2d):
  entity[Velocity].v = v

defMsg(Velocity, test1) do (x:var seq[int]):
  x.add Velocity.componentID


## safety tests

## components A, B, C
## B requires A
## C conflicts with B

type
  X_A = object
  X_B = object
  X_C = object

X_B.requiresComponent(X_A)
X_C.conflictsWithComponent(X_B)

var ty1 = newTypeinfo(componentIDs(X_B, X_C))
assert ty1.is_valid == false
var ty2 = newTypeinfo(componentIDs(X_B))
assert ty2.is_valid == false



proc does_nothing* [T] (some: T): void = discard
template voidExpr* (x): expr = not compiles(does_nothing(x))
  # hack to determine if an expression is void

template test (suite; body:stmt):stmt {.immediate.} =
  when not definedInScope(test_failures):
    var test_failures: seq[string] = @[]

  template testFailed(expression): stmt =
    when defined(ContinueRunningOnFailure) and ContinueRunningOnFailure:
      test_failures.safeadd astToStr(expression)
    else:
      echo suite, " [Failed] ", astToStr(expression)
      break
  
  template check (expression): stmt =
    if not expression:
      testFailed(expression)
      
      
  template shouldRaise (expression; exception): stmt =
    try:
      when voidExpr(expression):
        expression
      else:
        discard expression
      testFailed(expression.shouldRaise(exception)) # repeat the condition here so testFailed gets a nice expression
    except:
      check( getCurrentException() of exception )
  
  block:
    test_failures.setLen 0
    body
    if test_failures.len > 0:
      echo suite, " [Failed (", test_failures.len, ")]"
      for f in test_failures:
        echo "  ", f
    else:
      echo suite, " [Passed]"

defMsg(X_A, test1) do (x:var seq[int]):
  x.add X_A.componentID

test "instantiation test":
  const ContinueRunningOnFailure = true
  ty1.instantiate.shouldRaise EBadEntity
  ty2.instantiate.shouldRaise EBadEntity


try:
  discard ty1.instantiate
except:
  let x = getCurrentException().RBadEntity
  for error in x.errors.val:
    echo error


let dom = newDomain()

block:
  var ent1 = dom.newEntity(Position, X_A, Velocity)
  var comps: seq[int] = @[]
  ent1.test1(comps)
  assert comps == componentIDs(Position, X_A, Velocity)

  echo ent1.test_unhandled, " should be 42"
  ent1.test_unhandled_m

static:echo "Compile-time message info:"
dump_messages()

echo "Run-time message info:"
dump_messages_rt(id,name,multicast):
  echo "#",id, " ",name," ", (if multicast: "multicast" else: "unicast")


