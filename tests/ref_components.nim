discard """
example/test to make sure the GC can handle reference type components.
because components are stored in block memory, a GC_unref is needed in
a reference component type's destructor. when the entity gets destroyed
the component will then be unreffed and the memory can be freed.
"""

import entoody,strutils

type RefComponent = ref object
  x: int

proc newRefComponent (x): RefComponent =
  new(result) do (rc: RefComponent):
    echo "Freeing RefComponent ", rc.x
  result.x = x

componentInfo(RefComponent).destructor = proc(ent:PEntity) =
  "unreffing RefComponent $# (refcount: $#)".format(
    ent[RefComponent].x, 
    ent[RefComponent].getRefCount
  ).echo
  GC_unref ent[RefComponent]



var ents: array[5, PEntity]
var d = newDomain()


for i in 0 .. <5:
  var e = d.newEntity(RefComponent)
  e[RefComponent] = newRefComponent(i)
  #ents[i] = e
 
echo "entities created, running gc_fullcollect()"

for i in 1 .. 2:
  echo "Pass ", i
  GC_fullcollect()

if not ents[3].isNil:
  echo ents[3][RefComponent].repr

