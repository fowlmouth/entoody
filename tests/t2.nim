import entoody, basic2d

type
  Pos = object
    x,y: float

proc get_pos: TPoint2d {.unicast.}
proc set_pos (p:TPoint2d){.unicast.}


def_msg(Pos, get_pos) do -> TPoint2d:
  return point2d(entity[Pos].x, entity[Pos].y)
def_msg(Pos, set_pos) do (p:TPoint2d):
  entity[pos].x = p.x
  entity[pos].y = p.y

type
  Orientation = object
    radians: float

proc set_angle* (rad: float) {.unicast.}
def_msg(Orientation, set_angle) do (rad:float):
  entity[Orientation].radians = rad

type
  Sprite = object
    texture: pointer

type Window = object # example
proc draw* (w: Window) {.multicast.}

Sprite.setDestructor do (x:PEntity):
  echo "sprite destructor called for ent ", x.id
  
def_msg(Sprite, draw) do (w:Window): 
  #w.draw_texture x[Sprite].texture
  echo "WOW! I'm being drawn! ent id = ", entity.id


dump_components
dump_messages

let dom = newDomain()
let ty = dom.getTypeinfo(Pos,Orientation,Sprite)
let ent = ty.instantiate
ent.id = 99
ent.set_pos point2d(10,10)
ent.set_angle deg180

ent[pos] = pos(x:0,y:0)

echo "raw entity pos: ", ent[pos].x.int, ", ", ent[pos].y.int
ent.draw(window())

echo msg_is_unicast("draw")
echo message_id("draw")

ent.destroy



