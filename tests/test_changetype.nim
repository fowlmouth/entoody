import entoody

proc inspect (result:var seq[string]){.multicast.}

type TA = object
    x: int
type TB = object
type TC = object

defMsg(TA,inspect) do (r:var seq[string]):
    r.add "TA"
defMsg(TB,inspect) do (r:var seq[string]):
    r.add "TB"
defMsg(TC,inspect) do (r:var seq[string]):
    r.add "TC"


when ismainmodule:

    template ee (xpr): stmt = 
        echo xpr," :: ",astToStr(xpr)

    let dom = newDomain()
    let ent = dom.newEntity(TA,TB)

    var results: seq[string] = @[]
    ent.inspect(results)
    ee results == @["TA","TB"]
    results.setLen 0

    ent[TA].x = 42 # check it after we change entity's type

    let new_ty = dom.modifyComponents(ent.ty, 
        add = [componentID(TC)],
        remove = [componentID(TB)]
    )
    ent.changeType(new_ty)

    ent.inspect(results)
    ee results == @["TA","TC"]

    ee ent[TA].x == 42
